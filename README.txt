Overview
========

Views Title Callback (VTC) creates a display extender plugin for Views that
allows Views administrators the ability to specify custom title callback in any
display. Callback value is stored in the display and than invoked every-time 
view is rendered.

VTC is intended for use by Views (i.e. site) administrators.

Scenarios
========

CPV is useful in cases where:

- Account page (Member I follow for own profile and Members followed by XXX
  while viewing another user's profile) 
- Displaying total number of results in title
- Displaying value from one of the filters (normal/faceted) like 
  Hotels in [Location]

Requirements
========

VTC requires Drupal 7 and Views 3. It also requires the Views UI module 
to be enabled, as VTC has no effect otherwise.

Usage
========

Enable the VTC module, then go to "admin/structure/views/settings/advanced" to 
enable the display extender, "Title Callback". 

In each views display, a "Title Callback" field will allow custom text to be 
added under the "Advanced" fieldset in the display settings.
