<?php

/**
 * @file
 * Contains the class to extend views display plugins.
 */

/**
 * The plugin that added additional setting to views edit form.
 *
 * @ingroup views_display_plugins
 */
class views_title_callback_plugin_display_extender_code extends views_plugin_display_extender {

  /**
   * Provide a form to edit options for this plugin.
   */
  public function option_definition() {
    $options = array();

    $options['title_callback'] = array(
      'default' => '',
    );

    return $options;
  }

  /**
   * Set the title_callback data member in View object if value is set using
   * display extender. This will be used later by hook_views_pre_render().
   */
  public function pre_execute() {
    $title_callback = $this->display->get_option('title_callback');
    if ($title_callback && function_exists($title_callback)) {
      $this->view->title_callback = $title_callback;
    }
  }

  /**
   * Provide the form to set new option.
   */
  public function options_form(&$form, &$form_state) {
    switch ($form_state['section']) {
      case 'title_callback':
        $form['#title'] .= t('Title Callback Setting');
        $form['title_callback'] = array(
          '#type' => 'textfield',
          '#description' => t('Custom title callback which will be invoked to get dynamic title.'),
          '#default_value' => $this->display->get_option('title_callback'),
        );
        break;
    }
  }

  /**
   * Inserts the code into the view display.
   */
  public function options_submit(&$form, &$form_state) {
    $new_option = $form_state['values']['title_callback'];
    switch ($form_state['section']) {
      case 'title_callback':
        $this->display->set_option('title_callback', $new_option);
        break;
    }
  }

  /**
   * Summarizes new option.
   *
   * Lists the fields as either 'Yes' if there is text or 'None' otherwise and
   * categorizes the fields under the 'Other' category.
   */
  public function options_summary(&$categories, &$options) {
    $new_option = check_plain(trim($this->display->get_option('title_callback')));
    if ($new_option) {
      // Do nothing, display it.
    }
    else {
      $new_option = t('None');
    }
    $options['title_callback'] = array(
      'category' => 'other',
      'title' => t('Title Callback'),
      'value' => $new_option,
      'desc' => t('Add some option.'),
    );
  }

}
