<?php

/**
 * @file
 * Implements the display extender to Title Callback field to displays.
 */

/**
 * Implements hook_views_plugins().
 */
function views_title_callback_views_plugins() {
  $path = drupal_get_path('module', 'views_title_callback');

  $plugins = array();

  $plugins['display_extender']['views_title_callback'] = array(
    'title' => t('Title Callback'),
    'help' => t('Enter the callback function name to get dynamic title from.'),
    'path' => $path,
    'handler' => 'views_title_callback_plugin_display_extender_code',
  );

  return $plugins;
}
